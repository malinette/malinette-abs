# malinette-abs

Set of Pure Data abstractions for the [Malinette](http://malinette.info) project.

## Architecture
The Malinette software is mainly two things:
- **malinette-abs**: a set of abstractions to use Pure Data easily;
- **malinette-ide**: a framework and an interface to learn programming and for rapid prototyping;

Here, you are in the "malinette-abs" part, assuming that you use Pure Data. 

For newbies, you can try the "malinette-ide" interface or the **malinette-soft** version which is a standalone application with Pure Data and externals inside.


## Requirements
- Pure Data 0.48
- Externals :
	-lib : Gem zexy iemlib1* iemlib2 iem_t3_lib
	-path : bassemu~ comport cyclone ext13 Gem ggee hcs iemguts iemlib list-abs mapping maxlib moocow pix_fiducialtrack pmpd puremapping purepd sigpack tof zexy hid or hidin (windows)


## Helps
- Open 0-MANUAL.pd
- Visit http://malinette.info